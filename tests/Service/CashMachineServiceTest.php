<?php
/**
 * Created by PhpStorm.
 * User: sn0opr
 * Date: 1/13/18
 * Time: 2:41 PM
 */

namespace App\Tests\Service;

use App\Service\CashMachineService;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Constraint\IsType;

class CashMachineServiceTest extends TestCase
{
    private $cashNotes = [100, 50, 20, 10];

    /**
     * @dataProvider withdrawProvider
     * @param $amount
     * @throws \Exception
     */
    public function testWithdraw($amount)
    {
        $cashMachine = new CashMachineService($this->cashNotes);

        $notes = $cashMachine->withdraw($amount);
        $this->assertInternalType(IsType::TYPE_ARRAY, $notes);
        foreach ($notes as $note) {
            $this->assertInternalType(IsType::TYPE_FLOAT, $note);
        }
        $this->assertEquals(array_sum($notes), $amount);
    }

    /**
     * @throws \Exception
     * @expectedException \InvalidArgumentException
     */
    public function testNegativeAmount()
    {
        $cashMachine = new CashMachineService($this->cashNotes);
        $amount = -120;
        $cash = $cashMachine->withdraw($amount);
    }

    /**
     * @throws \Exception
     * @expectedException \App\Exception\NoteUnavailableException
     */
    public function testUnavailableNote()
    {
        $cashMachine = new CashMachineService($this->cashNotes);
        $amount = 155;
        $cash = $cashMachine->withdraw($amount);
    }

    /**
     * @return array
     */
    public function withdrawProvider()
    {
        return [
            [100.00],
            [20.00],
            [1250.00]
        ];
    }

    /**
     * @dataProvider initializeAvailableNotesProvider
     * @param $notes
     * @param $result
     */
    public function testInitializeAvailableNotes($notes, $result)
    {
        $cashMachine = new CashMachineService($notes);
        $initializedNotes = $cashMachine->getAvailableNotes();
        $this->assertTrue(array_diff($result, $initializedNotes) == []);
    }

    public function initializeAvailableNotesProvider()
    {
        return [
            [
                [100, 200, 50, 10, 20],
                [200, 100, 50, 20, 10]
            ],
            [
                [10, 1000, 500, 20],
                [1000, 500, 20, 10]
            ]
        ];
    }

}