<?php
/**
 * Created by PhpStorm.
 * User: sn0opr
 * Date: 1/13/18
 * Time: 2:03 PM
 */

namespace App\Controller;


use App\Exception\NoteUnavailableException;
use App\Service\CashMachineService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CashMachineController extends Controller
{
    /**
     * @var CashMachineService
     */
    private $cashMachine;

    public function __construct(CashMachineService $cashMachine)
    {
        $this->cashMachine = $cashMachine;
    }

    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('cashmachine/index.html.twig');
    }

    public function withdrawAction(Request $request)
    {
        $amount = floatval($request->get('amount', 0));
        try {
            $notes = $this->cashMachine->withdraw($amount);
            return new JsonResponse([
                'amount' => $amount,
                'notes' => $notes
            ]);
        } catch (\InvalidArgumentException | NoteUnavailableException $e) {
            return new JsonResponse([
                'error' => [
                    'code' => 400,
                    'message' => $e->getMessage()
                ]
            ], 400);
        }
    }
}