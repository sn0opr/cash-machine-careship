<?php
/**
 * Created by PhpStorm.
 * User: sn0opr
 * Date: 1/13/18
 * Time: 6:12 PM
 */

namespace App\Exception;

class NoteUnavailableException extends \Exception
{
    public function __construct(float $amount)
    {
        parent::__construct("Unavailable note for $amount");
    }
}