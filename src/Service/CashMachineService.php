<?php
/**
 * Created by PhpStorm.
 * User: sn0opr
 * Date: 1/13/18
 * Time: 2:05 PM
 */

namespace App\Service;


use App\Exception\NoteUnavailableException;

class CashMachineService
{
    /**
     * @var array
     */
    private  $availableNotes;

    public function __construct(array $availableNotes)
    {
        $this->initializeAvailableNotes($availableNotes);
    }

    /**
     * @param float $amount
     * @return float[]
     * @throws \InvalidArgumentException | NoteUnavailableException
     */
    public function withdraw(float $amount): array
    {
        if ($amount < 0 || !is_numeric($amount))
            throw new \InvalidArgumentException("The amount must be a positive number");
        $cash = [];
        $am = $amount;
        foreach ($this->availableNotes as $note) {
            if ($amount>= $note) {
                $times = floor($amount / $note);
                $this->addToCash($cash, $note, $times);
                $amount -= $note * $times;
            }
            if ($amount == 0)
                break;
        }
        if ($amount != 0)
            throw new NoteUnavailableException($am);
        return $cash;
    }

    /**
     * puts a $note to a given $cash array $n times
     * @param $cash
     * @param $note
     * @param $n
     */
    private function addToCash(array &$cash, float $note, int $n)
    {
        for($i = 0; $i < $n; $i++)
            $cash[] = $note;
    }

    public function  initializeAvailableNotes($notes)
    {
        usort($notes, function (&$a, &$b) {
            if (!is_numeric($a) || !is_numeric($b))
                throw new \InvalidArgumentException('Available notes must be float value');
            $a = floatval($a);
            $b = floatval($b);
            return $a < $b;
        });

        $this->availableNotes = $notes;
    }

    public function getAvailableNotes(): array
    {
        return $this->availableNotes;
    }

}