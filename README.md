#Cash Machine Case Study
I used Symfony 4 and VueJS for this project.
* Project tested on php 7.1
##Installation
* run `composer install`
* run `yarn install` or `npm install`
* run `./node_modules/.bin/encore dev` to generate the js/css with webpack (for dev env)
* run `./bin/phpunit` to run unit tests (for the first time, it will install some dependencies)
* open a http server with `./bin/console server:run *:<any port>` and open the url in the browser

##Unit tests
* Unit tests code are under `/tests` folder, I made unit tests for the `CashMachineService` class

Project created by Alaeddine Rihane for Careship.de test.