import '../sass/app.scss';
import Vue from 'vue';
import VueResource from 'vue-resource';

import App from './app.vue';
import CashMachine from './cashmachine.vue';

Vue.use(VueResource);
Vue.http.options.root = BASE_URL;

Vue.component('cash-machine', CashMachine);

new Vue({
    el: '#app',
    render: h => h(App)
});